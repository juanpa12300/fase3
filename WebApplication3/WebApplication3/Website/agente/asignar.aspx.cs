﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace WebApplication3.Website.agente
{
    public partial class asignar : System.Web.UI.Page
    {
        MySqlConnection conection = new MySqlConnection("server = localhost; Uid = root; password = 'jp'; database = fase2");

        public object region { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void Button2_Click(object sender, EventArgs e)
        {
            conection.Open();

            String tipo = Convert.ToString(DropDownList2.Text);
            String cod1 = Convert.ToString(TextBox1.Text);
            String cod2 = Convert.ToString(TextBox2.Text);

            MySqlCommand cmd = conection.CreateCommand();
            MySqlCommand cmd2 = conection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd2.CommandType = CommandType.Text;
            cmd.CommandText = "select * from "+tipo+" where codigo='" + TextBox1.Text + "'";
            cmd2.CommandText = "select * from tecnico where codigo='" + TextBox2.Text + "'";
            cmd.ExecuteNonQuery();
            cmd2.ExecuteNonQuery();
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            MySqlDataAdapter da2 = new MySqlDataAdapter(cmd2);
            da.Fill(dt);
            da2.Fill(dt2);
            foreach (DataRow dr in dt.Rows)
            {
                Session["codigo"] = dr["codigo"].ToString();
                TextBox3.Text = dr["nombre"].ToString();
            }

            foreach (DataRow dr2 in dt2.Rows)
            {
                Session["codigo"] = dr2["codigo"].ToString();
                TextBox4.Text = dr2["nombre"].ToString();
            }

            conection.Close();

            if(cod1.Equals("") || cod2.Equals("") || tipo.Equals(""))
            {
                Label1.Text = "Hay algun campo vacio, Verifica.";
            }
            else
            {
               
                Button1.Visible = true;
            }
            
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            conection.Open();
            String tipo = Convert.ToString(DropDownList2.Text);
            String cod1 = Convert.ToString(TextBox1.Text);
            String cod2 = Convert.ToString(TextBox2.Text);
            String nombre_emp = Convert.ToString(TextBox3.Text);
            String nombre_tec = Convert.ToString(TextBox4.Text);
            String estado = "En espera para la visita tecnica.";

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conection;
            cmd.CommandText = "INSERT INTO empresa(tipo, estado, nombre, tecnico, codtec) VALUES(?, ?, ?, ?, ?)";

            cmd.Parameters.Add("tipo", MySqlDbType.VarChar).Value = tipo;
            cmd.Parameters.Add("estado", MySqlDbType.VarChar).Value = estado;
            cmd.Parameters.Add("nombre", MySqlDbType.VarChar).Value = nombre_emp;
            cmd.Parameters.Add("tecnico", MySqlDbType.VarChar).Value = nombre_tec;
            cmd.Parameters.Add("codtec", MySqlDbType.VarChar).Value = cod2;
            cmd.ExecuteNonQuery();

            conection.Close();


            Label1.ForeColor = System.Drawing.Color.Green;
            Label1.Text = "Asignacion exitosa.";
        }
    }
}