﻿<%@ Page Language="C#" AutoEventWireup="true" Codefile="museos.aspx.cs" Inherits="WebApplication3.Website.museos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title></title>
<style type="text/css">
    #header {
				margin:auto;
				width: 1050px;
				font-family:Arial, Helvetica, sans-serif;
			}
			
			ul, ol {
				list-style:none;
			}
			
			.nav > li {
				float:left;
			}
			
			.nav li a {
				background-color:#052a52;
				color:#fff;
				text-decoration:none;
				padding:15px 40px;
				display:block;
			}
			
			.nav li a:hover {
				background-color:#434343;
			}
			
			.nav li ul {
				display:none;
				position:absolute;
				min-width:140px;
			}
			
			.nav li:hover > ul {
				display:block;
			}
			
			.nav li ul li {
				position:relative;
			}
			
			.nav li ul li ul {
				right:-215px;
				top:0px;
			}
            h1{
                text-align: center;
            }
            #cuerpo{
                text-align: center;
                font-size: 25px;
            }
            #Button1{
                text-align: center;
            }
        .auto-style1 {
        font-size: 20pt;
    }
        .auto-style2 {
        font-size: 30pt;
    }
        .auto-style4 {
        text-align: center;
    }
    .auto-style5 {
        font-size: 20pt;
        color: #00FF00;
    }
        </style>
</head>
<body>
     <form id="form1" runat="server">
        <div id="header">
			<ul class="nav">
				<li><a href="inicio.aspx">Inicio</a></li>
                <li><a href="galeria.aspx">Lugares de Turismo</a></li>
                <li><a href="Admin/admin.aspx">Administrador</a></li>
                <li><a href="">Empleados</a>
                    <ul>
						<li><a href="tecnico/tecnico.aspx">Técnico</a></li>
						<li><a href="agente/agente.aspx">Agente Turístico</a></li>
					</ul>
                </li>
				<li><a href="">Formularios</a>
					<ul>
						<li><a href="restaurantes.aspx">Restaurantes</a></li>
						<li><a href="hoteles.aspx">Hoteles</a></li>
						<li><a href="museos.aspx">Museos</a></li>
					</ul>
				</li>

				<li><a href="inicio.aspx">Acerca de</a></li>
			</ul>
		</div>
        <br />
        <br />
        <br />
        <br />

    <h1 class="auto-style2">Formulario para Museos</h1>
        <p class="auto-style4">
            <asp:Label ID="Label2" runat="server" CssClass="auto-style5" Text=" "></asp:Label>
         </p>

        <div id="cuerpo"> 
            Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox1" runat="server" Height="26px" Width="142px"></asp:TextBox>
            <br />
            <br />
            <br />
            Dirección&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox2" runat="server" Height="26px" Width="143px"></asp:TextBox>
            <br />
            <br />
            <br />
            Teléfono&nbsp;&nbsp;&nbsp; &nbsp; <asp:TextBox ID="TextBox3" runat="server" Height="26px" Width="140px"></asp:TextBox>
            <br />
            <br />
            <br />
            Tarifa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox4" runat="server" Height="27px" Width="147px"></asp:TextBox>
            <br />
            <br />
            <br />
            Hora inicio&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox5" runat="server" Height="26px" Width="142px"></asp:TextBox>
            <br />
            <br />
            <br />
            Hora cierre&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox6" runat="server" Height="26px" Width="153px"></asp:TextBox>
            <br />
            <br />
            Region&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:DropDownList ID="DropDownList1" runat="server" Height="29px" Width="133px">
                <asp:ListItem>Norte</asp:ListItem>
                <asp:ListItem>Sur</asp:ListItem>
                <asp:ListItem>Este</asp:ListItem>
                <asp:ListItem>Oeste</asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" CssClass="auto-style1" Height="41px" Text="Enviar Solicitud" Width="271px" OnClick="Button1_Click" />
            <br />
            <br />
            <br />

        </div>
    </form>
</body>
</html>