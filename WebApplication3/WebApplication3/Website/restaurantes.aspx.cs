﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace WebApplication3.Website
{
    public partial class restaurantes : System.Web.UI.Page
    {
        MySqlConnection conection = new MySqlConnection("server = localhost; Uid = root; password = 'jp'; database = fase2");


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            conection.Open();
            String nombre = Convert.ToString(TextBox1.Text);
            String region = Convert.ToString(DropDownList1.Text);
            String email = Convert.ToString(TextBox4.Text);
            String direccion = Convert.ToString(TextBox2.Text);
            String telefono = Convert.ToString(TextBox3.Text);

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conection;
            cmd.CommandText = "INSERT INTO restaurante(nombre, region, telefono, direccion, email) VALUES(?,?,?,?,?)";

            cmd.Parameters.Add("nombre", MySqlDbType.VarChar).Value = nombre;
            cmd.Parameters.Add("region", MySqlDbType.VarChar).Value = region;
            cmd.Parameters.Add("telefono", MySqlDbType.VarChar).Value = telefono;
            cmd.Parameters.Add("direccion", MySqlDbType.VarChar).Value = direccion;
            cmd.Parameters.Add("email", MySqlDbType.VarChar).Value = email;
            cmd.ExecuteNonQuery();

            conection.Close();

            Label1.Text = "Se envio correctamente la solicitud.";
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
            TextBox4.Text = "";
        }
    }
}