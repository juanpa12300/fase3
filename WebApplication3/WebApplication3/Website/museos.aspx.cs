﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace WebApplication3.Website
{
    public partial class museos : System.Web.UI.Page
    {
        MySqlConnection conection = new MySqlConnection("server = localhost; Uid = root; password = 'jp'; database = fase2");

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            conection.Open();
            String nombre = Convert.ToString(TextBox1.Text);
            String direccion = Convert.ToString(TextBox2.Text);
            String telefono = Convert.ToString(TextBox3.Text);
            String tarifa = Convert.ToString(TextBox4.Text);
            String hora1 = Convert.ToString(TextBox5.Text);
            String hora2 = Convert.ToString(TextBox6.Text);
            String region = Convert.ToString(DropDownList1.Text);

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conection;
            cmd.CommandText = "INSERT INTO museo(nombre, direccion, telefono, tarifa, hora1, hora2, region) VALUES(?,?,?,?,?,?,?)";

            cmd.Parameters.Add("nombre", MySqlDbType.VarChar).Value = nombre;
            cmd.Parameters.Add("direccion", MySqlDbType.VarChar).Value = direccion;
            cmd.Parameters.Add("telefono", MySqlDbType.VarChar).Value = telefono;
            cmd.Parameters.Add("tarifa", MySqlDbType.VarChar).Value = tarifa;
            cmd.Parameters.Add("hora1", MySqlDbType.VarChar).Value = hora1;
            cmd.Parameters.Add("hora2", MySqlDbType.VarChar).Value = hora2;
            cmd.Parameters.Add("region", MySqlDbType.VarChar).Value = region;
            cmd.ExecuteNonQuery();

            conection.Close();

            Label2.Text = "Se envio correctamente la solicitud.";
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
            TextBox4.Text = "";
            TextBox5.Text = "";
            TextBox6.Text = "";
        }
    }
}