﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login-admin.aspx.cs" Inherits="WebApplication3.Website.login_admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

         html{
            background: url(img/fondo.jpg); 
            width: 100%;
            height: 100%;
        }
         body{
            margin-top: 20px;
        }
         #header {
				margin:auto;
				width: 1050px;
				font-family:Arial, Helvetica, sans-serif;
			}
			
			ul, ol {
				list-style:none;
			}
			
			.nav > li {
				float:left;
			}
			
			.nav li a {
				background-color:#052a52;
				color:#fff;
				text-decoration:none;
				padding:15px 40px;
				display:block;
			}
			
			.nav li a:hover {
				background-color:#434343;
			}
			
			.nav li ul {
				display:none;
				position:absolute;
				min-width:140px;
			}
			
			.nav li:hover > ul {
				display:block;
			}
			
			.nav li ul li {
				position:relative;
			}
			
			.nav li ul li ul {
				right:-215px;
				top:0px;
			}
			

        h1{
            text-align: center;
        }

        .auto-style2 {
            text-decoration: none;
        }
        .auto-style3 {
            font-size: large;
        }
            h1{
                text-align: center;
                font-size: 35px;
            }
            #cuerpo{
                text-align: center;
                font-size: 25px;
            }
            #Button1{
                text-align: center;
            }

        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="header">
			<ul class="nav">
				<li><a href="">Inicio</a></li>
                <li><a href="">Lugares de Turismo</a></li>
                <li><a href="">Administrador</a></li>
                <li><a href="">Empleados</a></li>
				<li><a href="">Formularios</a>
					<ul>
						<li><a href="">Restaurantes</a></li>
						<li><a href="">Hoteles</a></li>
						<li><a href="">Museos</a></li>
					</ul>
				</li>

				<li><a href="">Acerca de</a></li>
			</ul>
		</div>
        <br />
        <br />
        <br />
        <br />

    <h1>Inicio de Sesión</h1>
        <p>&nbsp;</p>

        <div id="cuerpo"> 
            Usuario&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox1" runat="server" Height="23px" Width="221px"></asp:TextBox>
            <br />
            <br />
            <br />
            Password&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox2" runat="server" Height="24px" Width="223px"></asp:TextBox>
            <br />
            <br />
            <br />
            <br />

            <asp:Button ID="Button1" runat="server" Height="38px" Text="Entrar" Width="196px" OnClick="Button1_Click" />

        </div>
    </form>
    
</body>
</html>
